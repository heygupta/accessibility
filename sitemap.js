module.exports = {
    siteUrl: process.env.SITE_URL || 'https://accessibility-two.vercel.app/',
    generateRobotsTxt: true,
    sitemapSize: 7000,
  }