module.exports = {
  reactStrictMode: true,
  //   i18n: {
  //     locales: ["en", "es", "nl"],
  //     defaultLocale: "en",
  //  },

  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
}
